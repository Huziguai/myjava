package com.project.myjava.dao;

import com.project.myjava.model.Test;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TestMapper {

    public Test getTestById(@Param("id") String id);

}
