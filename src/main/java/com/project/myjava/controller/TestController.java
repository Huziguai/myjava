package com.project.myjava.controller;

import com.project.myjava.mq.Sender;
import com.project.myjava.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TestController {

    @Resource
    private TestService testService;

    @Autowired
    private Sender sender;

    @GetMapping("/getTestById")
    public void getTestById(@RequestParam("id") String id){
        System.out.println(testService.getTestById(id));
    }

    @GetMapping("/send")
    public void sendMqMsg(){
        sender.sender(testService.getTestById("1").toString());
    }

    @GetMapping("/fanoutSend")
    public void fanoutSend(){
        sender.fanoutSender(testService.getTestById("1").toString());
    }

    @GetMapping("/topicSend")
    public void topicSend(){
        sender.topicSender(testService.getTestById("1").toString());
    }

    @GetMapping("/topicSend2")
    public void topicSend2(){
        sender.topicSender2(testService.getTestById("1").toString());
    }
}
