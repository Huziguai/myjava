package com.project.myjava.service.impl;

import com.project.myjava.dao.TestMapper;
import com.project.myjava.model.Test;
import com.project.myjava.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    public TestMapper testMapper;

    @Override
    public Test getTestById(String id) {
        return testMapper.getTestById(id);
    }
}
