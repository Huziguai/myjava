package com.project.myjava.service;

import com.project.myjava.model.Test;
import org.springframework.stereotype.Service;

@Service
public interface TestService {

    public Test getTestById(String id);

}
