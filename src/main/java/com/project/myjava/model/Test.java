package com.project.myjava.model;

import lombok.Data;

import java.sql.Date;

@Data
public class Test{
    private String id;
    private String name;
    private String value;
    private Date created;
}
