package com.project.myjava.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {

    @Bean
    public Queue paymentNotifyQueue() {
        return new Queue("notify.payment");
    }

    /**
     * 1：声明注册 fanout 模式的交换机
     * @return FanoutExchange
     */
    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("fanout_test_exchange",true,false);
    }

    /**
     * 2：声明队列 test.fanout.queue
     *
     * durable:是否持久化,默认是false,持久化队列：会被存储在磁盘上，当消息代理重启时仍然存在，暂存队列：当前连接有效
     * exclusive:默认也是false，只能被当前创建的连接使用，而且当连接关闭后队列即被删除。此参考优先级高于durable
     * autoDelete:是否自动删除，当没有生产者或者消费者使用此队列，该队列会自动删除。
     * return new Queue("TestDirectQueue",true,true,false);
     *
     * 一般设置一下队列的持久化就好,其余两个就是默认false
     *
     * @return Queue
     */
    @Bean
    public Queue testQueue () {
        return new Queue("test.fanout.queue",true);
    }

    /**
     * 3：完成绑定关系（队列和交换机完成绑定关系）
     * @return Binding
     */
    @Bean
    public Binding testBinding() {
        return BindingBuilder.bind(testQueue()).to(fanoutExchange());
    }

    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange("topic_test_exchange",true,false);
    }

    @Bean
    public Queue topicQueue1 (){
        return new Queue("test.topic.queue1",true);
    }

    @Bean
    public Queue topicQueue2 (){
        return new Queue("test.topic.queue2",true);
    }

    //符号“#”表示通配符可以匹配0个或者多个单词。例如，“china.#”可以匹配所有的以“china”为前缀的主题，例如“china.beijing”，“china.shanghai.weather”等等。
    //
    //符号“ * ”表示通配符：可以匹配一个单词。例如，“china.*”可以匹配所有的以“china”为前缀并且后面只有一个单词的主题，例如“china.beijing”，“china.shanghai”，但是“china.shanghai.weather”不会被匹配。

    @Bean
    public Binding topicBing(){
        return BindingBuilder.bind(topicQueue1()).to(topicExchange()).with("test.*.#");
    }

    @Bean
    public Binding topicBing2() {
        return BindingBuilder.bind(topicQueue2()).to(topicExchange()).with("test.topic.key");
    }

}
