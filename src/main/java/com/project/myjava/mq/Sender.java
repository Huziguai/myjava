package com.project.myjava.mq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Sender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void sender(String msg){
        System.out.println("notify.payment send message: "+msg);
        rabbitTemplate.convertAndSend("notify.payment", msg);
    }

    /**
     * 广播模式
     */
    public void fanoutSender(String msg) {
        System.out.println("fanout_test_exchange send message: "+msg);
        //exchange：交换机名称
        //routingKey：路由键，fanout模式下不需要
        //Object： 传入数据
        rabbitTemplate.convertAndSend("fanout_test_exchange",null,msg);
    }

    public void topicSender(String msg){
        System.out.println("topic send message: "+msg);
        rabbitTemplate.convertAndSend("topic_test_exchange","test.topic.key", msg);
    }

    public void topicSender2(String msg){
        System.out.println("topic send message: "+msg);
        rabbitTemplate.convertAndSend("topic_test_exchange","test.topic", msg);
    }
}
