package com.project.myjava;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@MapperScan(basePackages = {"com.project.myjava.dao"})
public class MyjavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyjavaApplication.class, args);
    }
}
